// SPDX-License-Identifier: MIT
/*
Unified Unicode Utility
using Namespace
U[A-Za-z0-9-]+
is Reserved. please check a name conflicting.
and,
utf8_t, utf32_t is reserved.
*/

#ifndef UUUTIL_BASE_H
#define UUUTIL_BASE_H

#include <stddef.h>

#define UNICODE_MAXBIT 21

// Const for Length to Bits
const int UTF8_LENTOBIT[7] = {-1, 7, 11, 16, 21, 26, 31};

// bits per char
#define UTF8_BITPCHAR 6
// value masking bit
#define UTF8_BITCMASK 0x3f
// Head for first byte after
#define UTF8_BITMSBHD 0x80

typedef unsigned char utf8_t;
typedef unsigned int utf32_t;

enum UTFTypes {
	UTYPE_7, UTYPE_8, UTYPE_16, UTYPE_32, UTYPE_16BE, UTYPE_16LE, UTYPE_32BE, UTYPE_32LE
};

#endif
