// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define UTF8_USE_EXCEPTION

// If compiler does not inlining a `U8strlen', use this macro.
#define U8STRLEN(i,s) {\
for ((i) = CHAR_BIT-1; *s & (1<<i) && i > 0; i--);\
if (!(i)) (i) = -1;\
(i) = ((i) > 5) ? 1 : CHAR_BIT - i - 1;\
}

#include "uuubase.h"
#include "utf8.h"

#include <limits.h>

static inline unsigned char UTF8_MKHEAD(int len) {
	if (len < 2) return 0xFF;
	return -(0x80>>(len-1));
}

inline int uchrlen(utf8_t * chrp) {
	int i;
	
	// get a char length
#ifndef UTF8_USE_EXCEPTION
	for (i = CHAR_BIT-1; *chrp & (1<<i); i--);
#else
	for (i = CHAR_BIT-1; *chrp & (1<<i) && i > 0; i--);
	if (!i) return -1;
#endif
	return (i > 5) ? 1 : CHAR_BIT - i - 1;
}

// get a utf-8 char put in buf, return next utf8 char in str
inline char * uchrget(utf8_t * buf, utf8_t * str) {
	int len = uchrlen(str);
#ifdef UTF8_USE_EXCEPTION
	if (len == -1) return NULL;
#endif
	memcpy(buf, str, len);
	return (char*)str+len;
}

int uvalidate(utf8_t * chrp) {
	int len = Uchrlen(chrp);
	if (len < 0) return false;
	int i;
	for (i = 1; i < len; i++) {
		if ((chrp[i] & 0xc0) != 0x80) break;
	}
	return (i == len) ? true : false;
}

int uchrcnv32(utf32_t * dstp, utf8_t * srcp) {
	unsigned rawbuf;
	int i, len = U8chrlen(srcp);
	if (len < 0) return -1;
	rawbuf = (*srcp & ~-(0x80>>(unsigned)len)) << UTF8_BITPCHAR;
	
	for (i = 1; i < len; i++, rawbuf<<=UTF8_BITPCHAR) {
		if ((srcp[i] & 0xc0) == 0x80)
			rawbuf |= srcp[i] & 0x3f;
		else
			return -1;
	}
	*dstp = rawbuf;
	return len;
}
int uchrcnv8(utf8_t * dstp, utf32_t * srcp) {
	int i, j;
	
	if (*srcp <= CHAR_MAX) {
		*dstp = *srcp;
		return 1;
	}
	else if (*srcp <= 0xbf) return -1;
	
	if (*srcp >= 0xfc && *srcp <= 0xff) return -1;
	
	for (i = 11, j = 2; i < (int)(sizeof(utf32_t)*CHAR_BIT) && (*srcp & -(1<<i)); i+=5, j++);
	i = j;
	
	while (--j) {
		dstp[j] = (*srcp & UTF8_BITCMASK) | UTF8_BITMSBHD;
		*srcp &= ~UTF8_BITCMASK;
		*srcp >>= UTF8_BITPCHAR;
	}
	*dstp = UTF8_MKHEAD(i) | *srcp;
	return i;
}
int uconvto(void * dest, void * srce, enum UTF_ETypes);

int uchrcmp(utf8_t * cx, utf8_t * cy) {
	utf32_t x, y;
	Uchrcnv32(&x, cx), Uchrcnv32(&y, cy);
	return x - y;
}

// string.h functions
utf8_t * ustrcpy(utf8_t * dst, utf8_t * src) {
	size_t len = ustrlen(src);
	void * ret = memcpy(dst, src, len);
	dst[len] = '\0';
	return (utf8_t*)ret;
}
utf8_t * ustrcat(utf8_t * dst, utf8_t * src) {
	size_t dslen, sslen;
	dslen = ustrlen(dst);
	sslen = ustrlen(src);
	void * ret = memcpy(dst+dslen, src, sslen);
	dst[dslen+sslen] = '\0';
	return (utf8_t*)ret;
}
int	 ustrcmp(utf8_t * dst, utf8_t * src) {
	utf32_t dstraw, srcraw;
	
	while (*dst != '\0' && *src != '\0') {
		dst += uchrcnv32(&dstraw, dst);
		src += uchrcnv32(&srcraw, src);
		if (dstraw > srcraw)
			return 1;
	   else if (dstraw < srcraw)
			return -1;
	}
	
	return 0; // Equal
}
utf8_t * ustrdup(utf8_t * str) {
	return (utf8_t*)strdup((char*)str);
}
utf8_t * ustrchr(utf8_t * dst, utf8_t * src);
int	 ustrlen(utf8_t * str) {
	register size_t len, buf;
	
	for (len = buf = 0; *str != '\0'; str += buf, len += buf)
		buf = uchrlen(str);
	return len;
}

int main() {
	int c, i, len;
	//char buffer[BUFSIZ];
	
	char buf[8];
	buf[0] = 1;
	buf[7] = 0;
	c = 1;
	while (c) {
		printf("Input Unicode Hex : ");
		/*
		for (i = 0; i < 7; i++) {
			scanf("%x", &c);
			buf[i] = c;
			if (c == 0) break;
		}*/
		scanf("%x", &c);
		if ((len = uchrcnv8((utf8_t*)buf, (utf32_t*)&c)) < 0) {
			fprintf(stderr, "error: cannot encoding unicode.\n");
			return -1;
		}
		buf[len] = '\0';
		printf("UTF-8 : %s\n", buf);
		printf("Output Hexcode : ");
		for (i = 0; i < len; i++) {
			printf("%hhX ", buf[i]);
		}
		putchar('\n');
	}
	return 0;
}
