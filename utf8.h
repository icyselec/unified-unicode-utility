// SPDX-License-Identifier: MIT

#ifndef UUUTIL_UTF8_H
#define UUUTIL_UTF8_H

#include "uuubase.h"

// if error, return is below zero
// if need a string size, use strlen or strnlen

// Unicode Character Operation
int    uchrlen(utf8_t *);
int    uchrsiz(utf8_t *);
char * uchrget(utf8_t * buf, utf8_t * str);
int U8chrcmp(utf8_t *, utf8_t *);

// Charactor Encoding Convert


// Non Checking Boundary
utf8_t * ustrcpy(utf8_t *, utf8_t *);
utf8_t * ustrcat(utf8_t *, utf8_t *);
int	 ustrcmp(utf8_t *, utf8_t *);
utf8_t * ustrdup(utf8_t *);
utf8_t * ustrchr(utf8_t *, utf8_t *);
int	 ustrlen(utf8_t *);

// Checking Boundary
utf8_t * ustrncpy(utf8_t *, utf8_t *, size_t);
utf8_t * ustrncat(utf8_t *, utf8_t *, size_t);
int	 ustrncmp(utf8_t *, utf8_t *, size_t);
utf8_t * ustrndup(utf8_t *, size_t);
utf8_t * ustrnchr(utf8_t *, utf8_t *, size_t);
int	 ustrnlen(utf8_t *, size_t);

#endif
